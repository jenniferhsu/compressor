// ================================================================================
// name: compressor~.c
// desc: extern for pd
//          simple test that just outputs the input signal
//
//
//
// author: jennifer hsu
// date: fall 2013
// =================================================================================

#include "m_pd.h"
#include <stdlib.h>
#include <math.h>
#include <time.h>


// struct type to hold all variables that have to do with the compressor~ object
typedef struct _compressor
{
    t_object x_obj;
    t_float sr;
    t_int muteSamps;
    t_int muteSampCntr;
    t_float wetdry;
    t_float randomness;
    t_float levelEstimate;
    t_float dBThreshold;
} t_compressor;

// compressor~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class *compressor_class;

/* function prototypes */
static t_int *compressor_perform(t_int *w);
static void compressor_dsp(t_compressor *compressor_obj, t_signal **sp);
void compressor_wetdry(t_compressor *compressor_obj, t_floatarg f);
void compressor_randomness(t_compressor *compressor_obj, t_floatarg f);
static void *compressor_new(void);
void compressor_tilde_setup(void);


// perform function (audio callback)
static t_int *compressor_perform(t_int *w)
{
    
    // extract data
    t_compressor *compressor_obj = (t_compressor *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *out = (t_float *)(w[3]);
    int bufferSize = (int)(w[4]);
    
    // hardcode for now
    float attackTime = 0.001f;
    float releaseTime = 0.1f;
    float b0_a = 1.0f - exp(-1.0f / attackTime * compressor_obj->sr);
    float b0_r = 1.0f - exp(-1.0 / releaseTime * compressor_obj->sr);
    float comp_ratio = 0.5;
    
    // holders
    float dBLevelEstimate, dbGainVal, linGainVal;
    
    // TAKE CARE OF DRY WET LATER!
    
    
    // process samples
    int sample;
    for(sample = 0; sample < bufferSize; sample++)
    {
        // update level estimate
        float absIn = fabs(*(in+sample));
        if( absIn > compressor_obj->levelEstimate )
        {
            compressor_obj->levelEstimate += b0_a * (absIn - compressor_obj->levelEstimate);
        } else
        {
            compressor_obj->levelEstimate += b0_r * (absIn - compressor_obj->levelEstimate);
        }
        
        // output levelEstimate to check:
        // *(out+sample) = compressor_obj->levelEstimate;
        // it works!
        
        // get levelEstimate in dB
        if( compressor_obj->levelEstimate > 0.00001 )
        {
            // avoid taking log of 0
            dBLevelEstimate = 20.0f * log10f(compressor_obj->levelEstimate);
        } else
        {
            // set to -100dB
            dBLevelEstimate = -100.0f;
        }
        
        // check levelEstimate with user input level threshold
        if( compressor_obj->levelEstimate > compressor_obj->dBThreshold )
        {
            dbGainVal = 0.0f;
        } else
        {
            dbGainVal = ((1.0f / comp_ratio) - 1.0f) * (dBLevelEstimate - compressor_obj->dBThreshold);
        }
        
        // change gain into linear value
        linGainVal = powf( 10.0f,dbGainVal / 20.0f);
        
        // apply compressor gain to signal
        *(out+sample) = *(in+sample) * linGainVal;
    }
    
    
    return (w+5);
}

// called to start dsp, register perform function
static void compressor_dsp(t_compressor *compressor_obj, t_signal **sp)
{
    // save this here because I can't get it to work in the add function...
    compressor_obj->sr = sp[0]->s_sr;
    // add the perform function
    dsp_add(compressor_perform, 4, compressor_obj, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
    
    // reinitialize mute sample counter
    compressor_obj->muteSampCntr = 0;
    
}

// adjust wet/dry parameter - 0 means that it will be completely original file
// 100 means that it will be completely the stuttered signal
void compressor_wetdry(t_compressor *compressor_obj, t_floatarg f)
{
    // subtract from 1 so we don't have to do this in the perform function
    compressor_obj->wetdry = 1.0f - (f/100.f);
}

// adjust random parameter - 0 means that stuttering will never happen
// 100 means that stuttering will always happen
void compressor_randomness(t_compressor *compressor_obj, t_floatarg f)
{
    // subtract from 1 so we don't have to do this in the perform function
    compressor_obj->randomness = 1.0f - (f/100.f);
}


// new function that is called everytime we create a new compressor object in pd
static void *compressor_new(void)
{
    // create object from class
    t_compressor *compressor_obj = (t_compressor *)pd_new(compressor_class);
    
    // create a signal inlet for modulating the compression ratio
    inlet_new(&compressor_obj->x_obj, &compressor_obj->x_obj.ob_pd, &s_signal, &s_signal);
    
    // create control rate inlet for dry/wet parameter
    inlet_new(&compressor_obj->x_obj, &compressor_obj->x_obj.ob_pd, gensym("float"), gensym("wetdry"));
    
    // create a signal outlet for the output signal
    outlet_new(&compressor_obj->x_obj, gensym("signal"));
    
    compressor_obj->sr = 0.0f;
    
    // initialize samples to mute (hardcode to test this out)
    compressor_obj->muteSamps = 22050;
    // initialize wetdry value
    compressor_obj->wetdry = 0.0f;
    // initialize random/chance parameter
    compressor_obj->randomness = 0.0f;
    // intialize level estimate
    compressor_obj->levelEstimate = 0.0f;
    
    
    return (compressor_obj);
}

// called when Pd is first loaded and tells Pd how to load the class
void compressor_tilde_setup(void)
{
    compressor_class = class_new(gensym("compressor~"), (t_newmethod)compressor_new, 0,
                                sizeof(t_compressor), 0, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs delayTime as the leftmost inlet float
    CLASS_MAINSIGNALIN(compressor_class, t_compressor, sr);
    
    // register method that will be called when dsp is turned on
    class_addmethod(compressor_class, (t_method)compressor_dsp, gensym("dsp"), (t_atomtype)0);
    
    // add inlet for dry/wet parameter (control rate)
    class_addmethod(compressor_class, (t_method)compressor_wetdry, gensym("wetdry"), A_FLOAT, 0);
    
    // add inlet for randomness parameter (control rate)
    class_addmethod(compressor_class, (t_method)compressor_wetdry, gensym("randomness"), A_FLOAT, 0);
    
}

